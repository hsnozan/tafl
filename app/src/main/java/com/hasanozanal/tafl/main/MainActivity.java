package com.hasanozanal.tafl.main;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.hasanozanal.tafl.R;
import com.hasanozanal.tafl.scenes.home.fragment.HomeFragment;
import com.hasanozanal.tafl.pageradapter.ViewPagerAdapter;
import com.hasanozanal.tafl.util.view.AdjustableViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.main_view_pager)
    AdjustableViewPager viewPager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setUpViewPager();
    }

    private void setUpViewPager() {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new HomeFragment(), "Ana Sayfa");

        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setScrollable(false);

        tabLayout.setupWithViewPager(viewPager);
    }

    public int getContainer() {
        return R.id.main_container;
    }
}
