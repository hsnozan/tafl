package com.hasanozanal.tafl.scenes.addpost;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hasanozanal.tafl.R;
import com.hasanozanal.tafl.main.MainActivity;
import com.hasanozanal.tafl.scenes.home.model.HomePostListItem;
import com.hasanozanal.tafl.util.view.FragmentUtil;
import com.hasanozanal.tafl.util.view.ImagePicker;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.Constraints.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddPostFragment extends Fragment implements EasyPermissions.PermissionCallbacks {

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int PERMISSION_ALL = 19;
    private static final int QUALITY = 92;
    private static final int MAX_PROFILE_BITMAP_SIZE = 1024;
    public static final int REQUEST_CODE_PICKER_IMAGE = 777;
    public static final int PERMISSION_CODE_STORAGE = 444;

    @BindView(R.id.dummy_image)
    AppCompatImageView imageView;

    private DatabaseReference reference;
    private DatabaseReference newPost;
    private Uri filePath;
    private Uri downloadUri;

    public AddPostFragment() {
        // Required empty public constructor
    }

    public static AddPostFragment newInstance() {
        return new AddPostFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_post, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_PICKER_IMAGE && resultCode == RESULT_OK) {
            final Uri imageUri = ImagePicker.getImageFromResult(getContext(), data);
            startCropping(imageUri);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                filePath = result.getUri();
                try {
                    InputStream inputStream = getActivity().getContentResolver()
                            .openInputStream(filePath);
                    Bitmap profileBitmap = BitmapFactory.decodeStream(inputStream);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    profileBitmap = getResizedBitmap(profileBitmap, MAX_PROFILE_BITMAP_SIZE);
                    profileBitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY,
                            byteArrayOutputStream);
                    imageView.setImageBitmap(profileBitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @OnClick(R.id.dummy_image)
    public void onChooseButtonClick() {
        openCameraForProfilePicture();
    }

    @OnClick(R.id.upload_image_button)
    public void onUploadButtonClick() {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageReference = storage.getReference();
        if(filePath != null)
        {
            final StorageReference ref = storageReference.child("images/"+ UUID.randomUUID().toString());
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            ref.getDownloadUrl()
                                    .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    downloadUri = uri;
                                    newPost = reference.push();
                                }
                            });

                            referenceValueListener();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getContext(), "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void referenceValueListener() {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HomePostListItem homePostListItem = new HomePostListItem();
                homePostListItem.imageUrl = downloadUri.toString();
                homePostListItem.title = "deneme1234";
                newPost.setValue(homePostListItem)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    FragmentUtil.popLatestFragment((MainActivity) getActivity());
                                }
                            }
                        });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void setDatabaseReference(DatabaseReference reference) {
        this.reference = reference;
    }

    private void openCameraForProfilePicture() {
        String[] permissions = new String[0];
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        }

        if (!hasPermissions(getContext(), permissions)) {
            onCameraRequested();
        } else {
            requestCamera();
        }
    }

    private boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission)
                        != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }

        return true;
    }

    public void onCameraRequested() {
        if (getActivity() == null) {
            Log.w(TAG, "onCameraRequested: context is null");
            return;
        }

        String[] perms = new String[0];
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            perms = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        }

        if (EasyPermissions.hasPermissions(getActivity(), perms)) {
            requestCamera();
        } else {
            EasyPermissions.requestPermissions(new PermissionRequest
                    .Builder(this, PERMISSION_CODE_STORAGE, perms)
                    .setRationale(R.string.text_need_storage_access)
                    .setPositiveButtonText(R.string.text_ok)
                    .setNegativeButtonText(R.string.text_cancel)
                    .build());
        }
    }

    @AfterPermissionGranted(PERMISSION_CODE_STORAGE)
    private void requestCamera() {
        Intent chooseImageIntent = ImagePicker.getPickImageIntent(getContext());
        startActivityForResult(chooseImageIntent, REQUEST_CODE_PICKER_IMAGE);
    }

    private void startCropping(Uri imageUri) {
        CropImage.ActivityBuilder builder = CropImage.activity(imageUri)
                .setAllowRotation(false)
                .setFixAspectRatio(true)
                .setFlipHorizontally(false)
                .setAllowFlipping(false)
                .setMultiTouchEnabled(true)
                .setCropShape(CropImageView.CropShape.RECTANGLE);

        builder.start(getContext(), this);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions,
                grantResults, this);
    }
}
