package com.hasanozanal.tafl.scenes.home.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.hasanozanal.tafl.R;
import com.hasanozanal.tafl.scenes.home.model.HomePostListItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private ArrayList<HomePostListItem> postListModels = new ArrayList<>();

    public void setPostListArrayList(ArrayList<HomePostListItem> postListArrayList) {
        this.postListModels = postListArrayList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_home_list_item, parent,
                        false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        holder.bind(postListModels.get(i));
    }

    @Override
    public int getItemCount() {
        return postListModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_post_list_item_title)
        AppCompatTextView textView;
        @BindView(R.id.image_post_list_item)
        AppCompatImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bind(HomePostListItem homePostListItem) {
            textView.setText(homePostListItem.title);
            Glide.with(itemView.getContext())
                    .load(homePostListItem.imageUrl).into(imageView);
        }
    }
}
