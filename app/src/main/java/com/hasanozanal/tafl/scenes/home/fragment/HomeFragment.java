package com.hasanozanal.tafl.scenes.home.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hasanozanal.tafl.R;
import com.hasanozanal.tafl.main.MainActivity;
import com.hasanozanal.tafl.scenes.addpost.AddPostFragment;
import com.hasanozanal.tafl.scenes.home.adapter.HomeAdapter;
import com.hasanozanal.tafl.scenes.home.model.HomePostListItem;
import com.hasanozanal.tafl.util.view.FragmentUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements ValueEventListener {

    @BindView(R.id.recycler_view_home)
    RecyclerView homeRecyclerView;

    private HomeAdapter homeAdapter;
    private DatabaseReference reference;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        setHomeAdapter();
        setFirebaseDatabase();
        return view;
    }

    private void setHomeAdapter() {
        homeAdapter = new HomeAdapter();
        homeRecyclerView.setAdapter(homeAdapter);
        homeRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void setFirebaseDatabase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        reference = database
                .getReferenceFromUrl("https://tafl-b22b0.firebaseio.com/postList/postListItem");
        reference.addValueEventListener(this);
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<HomePostListItem> postListItem = new ArrayList<>();

        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            postListItem.add(snapshot.getValue(HomePostListItem.class));
        }

        homeAdapter.setPostListArrayList(postListItem);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Toast.makeText(getContext(), "" + databaseError, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button_start_add_post_fragment)
    public void onStartAddPostButtonClick() {
        MainActivity activity = (MainActivity) getActivity();

        if (activity != null) {
            AddPostFragment addPostFragment = AddPostFragment.newInstance();
            addPostFragment.setDatabaseReference(reference);
            FragmentUtil.startFragment(activity, activity.getContainer(), addPostFragment);
        }
    }
}
