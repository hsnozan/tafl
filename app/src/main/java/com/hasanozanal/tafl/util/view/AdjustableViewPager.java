package com.hasanozanal.tafl.util.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by hasanozanal on 17.03.2019.
 */

public class AdjustableViewPager extends ViewPager {

    private boolean isScrollEnabled = false;

    public AdjustableViewPager(Context context) {
        super(context);
    }

    public AdjustableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return isScrollEnabled && super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return isScrollEnabled && super.onInterceptTouchEvent(event);
    }

    public void setScrollable(Boolean isScrollable) {
        isScrollEnabled = isScrollable;
    }

}
