package com.hasanozanal.tafl.util.view;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.hasanozanal.tafl.main.MainActivity;

import java.util.List;

/**
 * Created by ozanal on 22.03.2019
 */
public final class FragmentUtil {

    private FragmentUtil() {

    }

    public static void startFragment(MainActivity activity, int container, Fragment fragment) {
        FragmentManager manager = activity.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(container, fragment);
        transaction.addToBackStack(fragment.getTag());
        transaction.commitAllowingStateLoss();
    }

    public static void popLatestFragment(MainActivity activity, Fragment fragment) {
        if (activity != null && fragment != null) {
            try {
                activity.getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            } catch (Exception e) {
                Log.i("BackStackCatch", "popBackStack can not handled");
            }
        }
    }

    public static void popLatestFragment(MainActivity activity) {
        if (activity != null) {
            try {
                activity.getSupportFragmentManager().popBackStackImmediate();
            } catch (Exception e) {
                Log.i("BackStackCatch", "popBackStack can not handled");
            }
        }
    }

    public static void clearFragmentStack(MainActivity activity) {
        if (activity != null) {
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            clearFragmentStack(fragmentManager);
        }
    }

    public static void clearFragmentStack(FragmentManager fragmentManager) {
        Integer stackCount = fragmentManager.getBackStackEntryCount();

        if (stackCount != null) {
            for (int i = 0; i < stackCount; ++i) {
                try {
                    fragmentManager.popBackStack();
                } catch (Exception e) {
                    Log.i("BackStackCatch", "popBackStack can not handled");
                }
            }
        }
    }

    public static boolean isVisibleToUser(MainActivity activity, Class fragmentClass) {
        if (activity == null) {
            return false;
        }

        List<Fragment> fragments = activity.getSupportFragmentManager().getFragments();

        return !fragments.isEmpty() && fragmentClass.isInstance(fragments.get(fragments.size() - 1));
    }

    public static void startPlayStore(MainActivity activity) {
        if (activity == null) {
            return;
        }

        String packageName = activity.getPackageName();

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://play.google.com/store/apps/details?id="
                + packageName));
        activity.startActivity(intent);
    }

}
